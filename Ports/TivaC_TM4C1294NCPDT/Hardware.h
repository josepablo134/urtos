/*
 * Hardware.h
 *
 *  Created on: 08/05/2019
 *      Author: josepablocb
 */

#ifndef HARDWARE_H_
#define HARDWARE_H_

    #include <stdint.h>
	#include <stdlib.h>
    #include <stdbool.h>
    #include "inc/hw_memmap.h"
    #include "inc/hw_gpio.h"
    #include "inc/hw_uart.h"

    #include "driverlib/debug.h"
    #include "driverlib/gpio.h"
    #include "driverlib/pwm.h"
    #include "driverlib/uart.h"
    #include "driverlib/sysctl.h"
    #include "driverlib/systick.h"
    #include "driverlib/pin_map.h"
    #include "driverlib/interrupt.h"

    extern void PWMInit( void );
    extern void PWMWrite( float );
    extern void EncoderInit( void );
    extern void EncoderStart( void );
    extern void EncoderSop( void );
    extern uint32_t EncoderRead( void );
    extern void EncoderClear( void );

	///	Configure UART
	extern void UARTInit( void );
	///	Transmit n bytes
	extern uint8_t UARTWrite( void* , uint32_t );
	extern uint8_t UARTWriteReady(void);
	///	Receive n bytes
	extern uint8_t UARTRead( void* , uint32_t );
    extern uint8_t UARTReadReady(void);
#endif /* HARDWARE_H_ */
