#ifndef SYSTEM_H_
#define SYSTEM_H_

    #include "uRTOS.h"
    #include "CompSci/PIDSystem.h"

    /// Proceso de mesurado
	#define	measureTaskPeriod	5
	extern void measureTask();

	///	Proceso de control
	#define	controlTaskPeriod	20
	extern void controlInit();
	extern void controlTask();

#endif /* SYSTEM_H_ */
