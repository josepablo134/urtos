/*
 * port.h
 *
 *  Created on: 14/01/2019
 *      Author: josep
 */

#ifndef PORT_H_
#define PORT_H_
    #include <stdint.h>
    #include <stdbool.h>
    typedef     uint32_t    portUBaseType_t;
    typedef     int32_t     portBaseType_t;
    typedef     uint32_t    portTickType_t;

    #define xPortWAIT_FOREVER           0xFFFFFFFF
    #define xPortMEM_MAX_HEAP_SIZE      1024


    #define     vPortEXEC_TASK()            { tskRunning->function(); }
    #define     vPort_Load_SystemContext()  {  }
    #define     vPort_Save_SystemContext()  {  }
    #define     vPort_Load_TaskContext()    {  }
    #define     vPort_Save_TaskContext()    {  }
    #define     vPortEXEC_TMR()             { tmrRunning->function(); }


    #include "driverlib/cpu.h"
    #define     vPortENTER_CRITICAL()       { CPUcpsid(); }
    #define     vPortEXIT_CRITICAL()        { CPUcpsie(); }

    #define     vPortDISABLE_INTERRUPTS()   { CPUcpsid(); }
    #define     vPortENABLE_INTERRUPTS()    { CPUcpsie(); }

    #define     vPortSYSTEM_CONTEXT_RESET() {  }

    #define     vPortCPU_SLEEP()            {  }
    #define     vPortCPU_RESET()            {  }


    #include "inc/hw_types.h"
    #include "inc/hw_memmap.h"
    #include "driverlib/sysctl.h"
    #include "driverlib/systick.h"
    #define     vPortCPU_CONFIG_SYSTICK()   {\
    SysCtlClockFreqSet((SYSCTL_XTAL_25MHZ |\
                                       SYSCTL_OSC_MAIN |\
                                       SYSCTL_USE_PLL |\
                                       SYSCTL_CFG_VCO_480), 120000000);\
    SysTickPeriodSet( 120000 );\
    SysTickIntEnable();\
    }

    #define     vPortCPU_START_SYSTICK()    { SysTickEnable(); }
    #define     vPortCPU_STOP_SYSTICK()     { SysTickDisable(); }

    #define     pdPORT_MS_TO_TICKS(TICKS)   TICKS
#endif /* PORT_H_ */
