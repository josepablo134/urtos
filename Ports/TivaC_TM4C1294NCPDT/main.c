#include "Hardware.h"
#include "System.h"

volatile uint32_t     CPUFREQ=0x0;

void __error_Handler__(){while(1);}
void SysTickIntHandler(){xTaskIncrementTick();}


extern TimerHandle_t    measureHdl;
extern TimerHandle_t    controlHdl;

volatile unsigned char   msg[]="Hello world from TivaC using uRTOS\n";

int main(void){
    uint32_t    counter;
    float       duty=0.0;
    //vTaskStartScheduler configura la frecuencia del procesador.
    //Ademas, el puerto UART utiliza el PIOSC directamente.
    CPUFREQ = SysCtlClockFreqSet((SYSCTL_XTAL_25MHZ |
                        SYSCTL_OSC_MAIN |
                        SYSCTL_USE_PLL |
                        SYSCTL_CFG_VCO_480), 120000000);
    IntMasterEnable();
    /// Configure Peripherals
    SysCtlPeripheralEnable( SYSCTL_PERIPH_GPION );
    SysCtlPeripheralEnable( SYSCTL_PERIPH_GPIOF );
    while( !SysCtlPeripheralReady(SYSCTL_PERIPH_GPION) ){}
    while( !SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOF) ){}
    GPIOPinTypeGPIOOutput( GPIO_PORTN_BASE , 0x03 );
    GPIOPinTypeGPIOOutput( GPIO_PORTF_BASE , GPIO_PIN_4 );

    PWMInit();
    EncoderInit();
    EncoderStart();
    UARTInit();
    controlInit();

    /// Configure uRTOS
    SysTickIntRegister( SysTickIntHandler );
    vTaskSetup();
    if( xTimerSetup() ){ goto SYSERROR; }

    measureHdl = xTimerCreate( measureTaskPeriod , pdTRUE , measureTask );
    if( !measureHdl ){ goto SYSERROR; }
    controlHdl = xTimerCreate( controlTaskPeriod , pdTRUE , controlTask );
    if( !controlHdl ){ goto SYSERROR; }

    xTimerStart( measureHdl , 0 );
    xTimerStart( controlHdl , 0 );
/*
    for( counter=0; counter<100; counter++ ){
        duty += 1.0;
        PWMWrite( duty );
        SysCtlDelay( 40000*10 );
    }
    for( counter=0; counter<100; counter++ ){
        duty -= 1.0;
        PWMWrite( duty );
        SysCtlDelay( 40000*10 );
    }
*/
    UARTCharGet( UART0_BASE );
    EncoderClear();
    vTaskStartScheduler();
SYSERROR:
    __error_Handler__();
}
