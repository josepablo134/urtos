#include "Hardware.h"
#include "inc/hw_ints.h"
#include "inc/hw_types.h"

#define     PWM_PERIOD  300
#define     PWM_FACTOR  (float)(PWM_PERIOD/100.0)
void PWMInit( void ){
    /// Activar el generador PWM
    SysCtlPeripheralEnable( SYSCTL_PERIPH_PWM0 );
    while(! SysCtlPeripheralReady( SYSCTL_PERIPH_PWM0 ) ){}
    /// Configurar pin PWM0-CH0
    GPIOPinConfigure(GPIO_PF0_M0PWM0);
    GPIOPinTypePWM( GPIO_PORTF_BASE , GPIO_PIN_0 );
    /// Configurar PWM0
    PWMGenConfigure( PWM0_BASE , PWM_GEN_0,
                     PWM_GEN_MODE_DOWN | PWM_GEN_MODE_NO_SYNC );
    /// Recordemos que el SYSCLOCK es de 120MHZ
    /// Si deseamos una frecuencia de 50KHz
    PWMClockSet( PWM0_BASE , PWM_SYSCLK_DIV_8 );
    PWMGenPeriodSet( PWM0_BASE , PWM_GEN_0, PWM_PERIOD );
    PWMGenEnable( PWM0_BASE , PWM_GEN_0 );
    PWMOutputState( PWM0_BASE , PWM_OUT_0_BIT , false );
}
void PWMWrite( float signal ){
    uint32_t    pwm_signal;
    if( signal > 0.0 ){
        ///Map 0-100 -> 0-400
        if( signal >= 100.0 ){
            pwm_signal = 400;
        }else{
            pwm_signal = (uint32_t)(signal*PWM_FACTOR);
        }
        PWMPulseWidthSet( PWM0_BASE , PWM_GEN_0, pwm_signal );
        PWMOutputState( PWM0_BASE , PWM_OUT_0_BIT , true );
        return;
    }
    PWMOutputState( PWM0_BASE , PWM_OUT_0_BIT , false );
}


#define		ENCODER_PERIPH	    SYSCTL_PERIPH_GPIOP
#define		ENCODER_PORT	    GPIO_PORTP_BASE
#define		ENCODER_PIN		    GPIO_PIN_0
#define     ENCODER_PIN_FB      GPIO_PIN_1

	volatile uint32_t	EncoderCounter;
	void EncoderIntHandler( void ){
        GPIOIntClear( ENCODER_PORT , ENCODER_PIN );
        HWREG(GPIO_PORTF_BASE + (GPIO_O_DATA + (GPIO_PIN_4 << 2))) ^= GPIO_PIN_4;
	    HWREG(ENCODER_PORT + (GPIO_O_DATA + (ENCODER_PIN_FB << 2))) ^= ENCODER_PIN_FB;
		EncoderCounter++;
	}
void EncoderInit( void ){
	EncoderCounter=0;
	///	Activar el periferico
	SysCtlPeripheralEnable( ENCODER_PERIPH );
	while(!SysCtlPeripheralReady( ENCODER_PERIPH ) ){}
	///	Registrar las interrupciones del pin
	GPIOPinTypeGPIOInput( ENCODER_PORT , ENCODER_PIN );
	GPIOPinTypeGPIOOutput( ENCODER_PORT , ENCODER_PIN_FB );
	GPIOPadConfigSet( ENCODER_PORT , ENCODER_PIN , GPIO_STRENGTH_12MA , GPIO_PIN_TYPE_STD_WPU );
	GPIOIntTypeSet( ENCODER_PORT , ENCODER_PIN , GPIO_BOTH_EDGES );
	GPIOIntRegister( ENCODER_PORT , EncoderIntHandler );
}
void EncoderStart( void ){
	GPIOIntEnable( ENCODER_PORT , ENCODER_PIN );
}
void EncoderStop( void ){
	GPIOIntDisable( ENCODER_PORT , ENCODER_PIN );
}
uint32_t EncoderRead( void ){
	register uint32_t EncoderRead;
	///	Acceso controlado a la variable compartida
	GPIOIntDisable( ENCODER_PORT , ENCODER_PIN );
		EncoderRead = EncoderCounter;
	GPIOIntEnable( ENCODER_PORT , ENCODER_PIN );
	return EncoderRead;
}
void EncoderClear( void ){
	///	Acceso controlado a la variable compartida
	GPIOIntDisable( ENCODER_PORT , ENCODER_PIN );
		EncoderCounter=0;
	GPIOIntEnable( ENCODER_PORT , ENCODER_PIN );
}


    typedef enum UART_TXSTATE_t{
        TX_READY,
        TX_BUSY,
        TX_STATE_LEN
    }UART_TXSTATE_t;
    typedef enum UART_RXSTATE_t{
        RX_READY,
        RX_BUSY,
        RX_STATE_LEN
    }UART_RXSTATE_t;
    volatile    UART_RXSTATE_t  RXSTATE;
    volatile    UART_TXSTATE_t  TXSTATE;
    volatile    uint8_t         *UARTTXBUFFER;
    volatile    uint32_t        UARTTXCOUNTER;
    volatile    uint8_t         *UARTRXBUFFER;
    volatile    uint32_t        UARTRXCOUNTER;
    void UARTIntHandler(){
        if( UARTIntStatus( UART0_BASE , UART_INT_TX ) ){
            UARTIntClear( UART0_BASE , UART_INT_TX );
            /// Enviar el siguiente byte
            if( TXSTATE == TX_BUSY ){
                if( UARTTXCOUNTER ){
                    UARTCharPutNonBlocking( UART0_BASE , *UARTTXBUFFER );
                    ///Avanzar en el arreglo
                    UARTTXBUFFER++;
                    UARTTXCOUNTER--;
                }else{
                    UARTIntDisable( UART0_BASE , UART_INT_TX );
                    TXSTATE = TX_READY;
                }
            }else{
                UARTIntDisable( UART0_BASE , UART_INT_TX );
                TXSTATE = TX_READY;
            }
        }
        if( UARTIntStatus( UART0_BASE , UART_INT_RX ) ){
            UARTIntClear( UART0_BASE , UART_INT_RX );
            /// Recibir el siguiente byte
            if( RXSTATE == RX_BUSY ){
                if( UARTTXCOUNTER ){
                    *UARTRXBUFFER = (uint8_t)UARTCharGetNonBlocking( UART0_BASE );
                    ///Avanzar en el arreglo
                    UARTRXBUFFER++;
                    UARTRXCOUNTER--;
                }else{
                    UARTIntDisable( UART0_BASE , UART_INT_RX );
                    RXSTATE = RX_READY;
                }
            }else{
                UARTIntDisable( UART0_BASE , UART_INT_RX );
                RXSTATE = RX_READY;
            }
        }
    }

	///	Configure UART
void UARTInit( void ){
    UARTRXCOUNTER = UARTTXCOUNTER = 0;
    UARTRXBUFFER = UARTTXBUFFER = 0x0;
    RXSTATE = RX_READY;
    TXSTATE = TX_READY;
	///	Configurar periferico
	SysCtlPeripheralEnable( SYSCTL_PERIPH_UART0 );
	SysCtlPeripheralEnable( SYSCTL_PERIPH_GPIOA );
	while(! SysCtlPeripheralReady( SYSCTL_PERIPH_UART0 ) ||
		  ! SysCtlPeripheralReady( SYSCTL_PERIPH_GPIOA )){}
	///	Configurar UART
    GPIOPinConfigure(GPIO_PA0_U0RX);
    GPIOPinConfigure(GPIO_PA1_U0TX);
    GPIOPinTypeUART(GPIO_PORTA_BASE, GPIO_PIN_0 | GPIO_PIN_1);
    UARTClockSourceSet( UART0_BASE , UART_CLOCK_PIOSC );
    UARTConfigSetExpClk(UART0_BASE, 16000000, 115200,
                            (UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE |
                             UART_CONFIG_PAR_NONE));
    UARTFIFODisable( UART0_BASE );
	/// Configurar Interrupciones
	UARTIntRegister( UART0_BASE , UARTIntHandler );
	//IntEnable( INT_UART0 );
	UARTIntEnable(UART0_BASE, UART_INT_RX );
	UARTIntClear( UART0_BASE , UART_INT_RX | UART_INT_TX );
}
///	Transmit n bytes
uint8_t UARTWrite( void* buffer, uint32_t size){
    UARTIntDisable( UART0_BASE , UART_INT_TX );
    if( TXSTATE == TX_BUSY ){
        UARTIntEnable( UART0_BASE , UART_INT_TX );
        return 1;
    }
    UARTTXBUFFER = (uint8_t*) buffer;
    UARTTXCOUNTER = size-1;
    TXSTATE = TX_BUSY;
    /// La interrupcion se encarga de la transmision
    UARTCharPutNonBlocking( UART0_BASE , *UARTTXBUFFER );
    UARTTXBUFFER++;
    UARTIntEnable( UART0_BASE , UART_INT_TX );
    return 0;
}
uint8_t UARTWriteReady( void ){
    register uint8_t    state;
    UARTIntDisable( UART0_BASE , UART_INT_TX );
    state = TXSTATE;
    UARTIntEnable( UART0_BASE , UART_INT_TX );
    return (state==TX_READY);
}
///	Receive n bytes
uint8_t UARTRead( void* buffer, uint32_t size){
    UARTIntDisable( UART0_BASE , UART_INT_RX );
    if( RXSTATE == RX_BUSY ){
        UARTIntEnable( UART0_BASE , UART_INT_RX );
        return 1;
    }
    UARTRXBUFFER = (uint8_t*) buffer;
    UARTRXCOUNTER = size;
    RXSTATE = RX_BUSY;
    /// La interrupcion se encarga de la recepcion
    UARTIntEnable( UART0_BASE , UART_INT_RX );
	return 0;
}
uint8_t UARTReadReady( void ){
    register uint8_t    state;
    UARTIntDisable( UART0_BASE , UART_INT_RX );
    state = RXSTATE;
    UARTIntEnable( UART0_BASE , UART_INT_RX );
    return (state==RX_READY);
}
