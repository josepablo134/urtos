/*
*	TODO:
*		- Encontrar el factor de lazo abierto.
*		- Crear graficas de velocidad en lazo abierto y cerrado para la misma entrada.
*		- Documentar y preparar para entregar.
*		- Describir ecuaciones si son necesarias, la funcion de transferencia de
*			lazo abierto y cerrado.
**/
#include "Hardware.h"
#include "System.h"
#include <string.h>
#include <stdio.h>

#define KP	40.0
#define KI	2.5
#define KD	1.25

TimerHandle_t  			controlHdl;
volatile PIDSystem_t 	ctl;
extern volatile var  	encoderSpeed,setpointMeas;

volatile var		    input,setpoint=3.1416;
const var               openLoopFactor=1.0;
typedef struct UART_PACKAGE{
    unsigned char   HEADER[7];
    var                  data;
}UART_PACKAGE;

volatile UART_PACKAGE            frame = {"HEADER",0.0};

void controlInit(){
	///Some initialization
	PIDSystemInit( (PIDSystem_t *) &ctl );
	ctl.Kp = KP;
	ctl.Ki = KI;
	ctl.Kd = KD;
	ctl.Ts = controlTaskPeriod/1000.0;
	input = 0.0;
}

void controlTask(){
    //HWREG(GPIO_PORTF_BASE + (GPIO_O_DATA + (GPIO_PIN_4 << 2))) ^= GPIO_PIN_4;
    /// Las variables de control se comparten entre procesos,
    /// debido a que el RTOS esta en modo no apropiativo,
    /// no se requiere ningun mecanismo de sincronizacion
	input 		= encoderSpeed;
	//setpoint    = setpointMeas;
	PWMWrite( PIDSystemCompute( input , setpoint , (PIDSystem_t *) &ctl ) * openLoopFactor );
//	PWMWrite( 50.0 );
	if( UARTWriteReady() ){
	    frame.data = (setpoint-input);
        UARTWrite( (void*) &frame , sizeof(UART_PACKAGE) );
	}
}
