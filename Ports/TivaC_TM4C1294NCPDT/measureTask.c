#include "Hardware.h"
#include "System.h"

#define                     TICKS_P_REV     20.0

const var                   Factor = 0.8965732459;//0.14269406392694065;
volatile var                encoderSpeed=0.0;
volatile var                setpointMeas=0.0;
TaskHandle_t    			measureHdl;

volatile uint32_t           encoderTicksMean[3]={0,0,0};
volatile uint8_t            counter=0;
volatile uint32_t           encoderTicks=0;

inline void incCounter(void){
    counter = (counter+1)%3;
}
void measureTask(){
    /*
        HWREG(GPIO_PORTN_BASE + (GPIO_O_DATA + (0x02 << 2))) ^= 0x02;
        encoderTicks = EncoderRead();
        EncoderClear();
        encoderSpeed = ((var)encoderTicks)*Factor;
        //encoderSpeed = (var)encoderTicks;
     */
    encoderTicksMean[ counter ] = EncoderRead();
    EncoderClear();

    encoderTicks = encoderTicksMean[counter];
    incCounter();
    encoderTicks = encoderTicksMean[counter];
    incCounter();
    encoderTicks = encoderTicksMean[counter];
    encoderSpeed = ((var)encoderTicks/3)*Factor;
}
