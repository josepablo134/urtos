#include "port.h"
#include "task.h"

void __interrupt (low_priority) SysTick_Handle(void){
    TMR1 += (SYSTICK_TIME);
	PIR1bits.TMR1IF = 0;
    /// Validar tareas en waiting;
    xTaskIncrementTick();
    return;
}

extern  TaskHandle_t       tskRunning;
volatile static uint16_t __temp_wreg;    
void EXEC_TASK(void){
    /// Subir ptr de function a pila
    vTaskENTER_CRITICAL();
        asm("PUSH");
        __temp_wreg = (uint16_t)(tskRunning->function);
        asm("MOVF   ___temp_wreg,W");
        asm("MOVWF  TOSL");
        asm("MOVF   ___temp_wreg+1,W");
        asm("MOVWF  TOSH");
        asm("CLRF  TOSU");
    vTaskEXIT_CRITICAL();
    //tskRunning->function();
    /// Retornar a la funcion
    return;
}

extern  TimerHandle_t       tmrRunning;
void EXEC_TMR(void){
    /// Subir ptr de function a pila
    vTaskENTER_CRITICAL();
        asm("PUSH");
        __temp_wreg = (uint16_t)(tmrRunning->function);
        asm("MOVF   ___temp_wreg,W");
        asm("MOVWF  TOSL");
        asm("MOVF   ___temp_wreg+1,W");
        asm("MOVWF  TOSH");
        asm("CLRF  TOSU");
    vTaskEXIT_CRITICAL();
    //tmrRunning->function();
    /// Retornar a la funcion
    return;
}