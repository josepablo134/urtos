#ifndef _PORT_H_
#define _PORT_H_
    #include "picconfig.h"

    #define TASK_STACK_MAX_SIZE     31

    //////////////////////////////////////////////
    //  CONTEXT SAVING
    //////////////////////////////////////////////
    #define vPortSYSTEM_CONTEXT_RESET(){ asm("CLRF   STKPTR"); }

    extern void EXEC_TASK(void);
    #define vPortEXEC_TASK(){   EXEC_TASK();    }

    extern void EXEC_TMR(void);
    #define vPortEXEC_TMR(){    EXEC_TMR();     }

    ///////////////////////////////////////////////
    //  INTERRUPT MODES
    ///////////////////////////////////////////////
    #define vPortENTER_CRITICAL(){  asm("BCF    INTCON,7");asm("BCF    INTCON,6"); }
    #define vPortEXIT_CRITICAL(){   asm("BSF    INTCON,7");asm("BSF    INTCON,6"); }

    #define vPortDISABLE_INTERRUPTS(){  asm("BCF    INTCON,7");asm("BCF    INTCON,6"); }
    #define vPortENABLE_INTERRUPTS(){   asm("BSF    INTCON,7");asm("BSF    INTCON,6"); }

    ///////////////////////////////////////////////
    //  BASIC POWER MODES
    ///////////////////////////////////////////////
    #define vPortCPU_SLEEP(){ asm("SLEEP"); }
    #define vPortCPU_RESET(){ asm("RESET"); }

    ///////////////////////////////////////////////
    //  SYSTICK
    ///////////////////////////////////////////////
    #define SYS_TICK_FREQ       12000000  //Internal OSC
    #define SYSTICK_US          1000     //Internal OSC
    //INTERNAL OSC  
//    #define SYSTICK_TIME        ~(uint16_t)((float)SYS_TICK_FREQ*SYSTICK_US/1000000)
    #define SYSTICK_TIME        65503//~((uint16_t)32)

    ///External OSC
    #define pdMS_TO_TICKS( Ticks )          Ticks 
    #define vPortCPU_CONFIG_SYSTICK(){ \
                                INTCONbits.GIE = 0;\
                                INTCONbits.GIEL = 1;\
                                RCONbits.IPEN = 1;\
                                IPR1bits.TMR1IP = 0;\
                                PIE1bits.TMR1IE = 1;\
                                PIR1bits.TMR1IF = 0;\
                                T1CON = 0b01001010;\
                                INTCONbits.GIE = 1;\
                                TMR1 = (SYSTICK_TIME);\
                            }
    
    ///Internal OSC
//    #define pdMS_TO_TICKS( Ticks )            Ticks 
//    #define vPortCPU_CONFIG_SYSTICK(){ \
//                                INTCONbits.GIE = 0;\
//                                INTCONbits.GIEL = 1;\
//                                RCONbits.IPEN = 1;\
//                                IPR1bits.TMR1IP = 0;\
//                                PIE1bits.TMR1IE = 1;\
//                                PIR1bits.TMR1IF = 0;\
//                                T1CON = 0b00000000;\
//                                INTCONbits.GIE = 1;\
//                                TMR1 = (SYSTICK_TIME);\
//                            }
    #define vPortCPU_START_SYSTICK(){ asm("BSF  T1CON,0"); }
    #define vPortCPU_STOP_SYSTICK(){ asm("BCF   T1CON,0"); }
    
    #define xPortWAIT_FOREVER       0xFFFF
#endif