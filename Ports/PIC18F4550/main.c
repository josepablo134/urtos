#pragma warning disable 520     //Function not used
#pragma warning disable 1498    //pointer could not set
//#pragma warning disable 1510    //pointer could not set
#include "uRTOS.h"

/// Manejador de hilo, necesario para el OS.
TaskHandle_t    Task0_Handle;
Task_t          Task1_struct;
TaskHandle_t    Task1_Handle;

TaskHandle_t    Task2_Handle;

/// Constantes creadas arbitrariamente
/// Definen el tiempo de Delay que ejecuta cada Hilo
#define TSK0_DELAY      pdMS_TO_TICKS(200)
#define TSK1_DELAY      pdMS_TO_TICKS(20000)
#define TSK2_DELAY      pdMS_TO_TICKS(500)

#define TMR0_PERIOD     pdMS_TO_TICKS(500)
#define TMR0_TIMOUT     pdMS_TO_TICKS(2000)

void Task0(void);
void Task1(void);
void Task2(void);

TimerHandle_t       Timer0_Handle;
void Timer0(void);

SemaphoreHandle_t   Sem0_Handle;

void main(void) {
    /// Inicializar el puerto A {
    PORTA = 0;
    LATA = 0b00000000;
    TRISA = 0b00000100;
    ADCON1= 0b00001110;
    CMCON = 0x07;
    /// Inicializar el puerto A }
        
    /// Configuraciones necesarias para los registros del SO
    vTaskSetup();
    if( uxTimerSetup() ){
        while(1);///Timer daemon not created
    }
    
    if(xTaskCreate( Task0 , 0xFF , tskNORMAL_PRIORITY , &Task0_Handle )){
        while(1);
    }
    
    Task1_Handle = xTaskCreateStatic( Task1 , 0xFA , tskLOW_PRIORITY , &Task1_struct );
    if( Task1_Handle == null ){
        while(1);
    }
    if(xTaskCreate( Task2 , 0xFF , tskLOW_PRIORITY , &Task2_Handle )){
        while(1);
    }
    
    Timer0_Handle = xTimerCreate(  TMR0_PERIOD,        ///Execute Timer0
                                   pdTRUE,  //every 4 SysTicks!
                                   Timer0); //
    if( !Timer0_Handle ){
        while(1);
    }
    xTimerStart(Timer0_Handle , TMR0_TIMOUT);       ///Start timer0 after 150 SysTicks!
    
    Sem0_Handle = xSemaphoreCreateBinary();
    if( !Sem0_Handle ){
        while(1);
    }
    
    /**
     * RTOS Trigger, this function never return.
     * After this function, the CPU must run the
     * first most higher priority task registered.
     */
    vTaskStartScheduler();
    
    
    /// Para que el Compilador sea feliz e incluya las funciones
    /// {
    Task0();
    Task1();
    Task2();
    Timer0();
    /// }
    /// Para que el Compilador sea feliz e incluya las funciones
    for(;;);
}

void Task0(void){
for(;;){
    if( xSemaphoreTake( Sem0_Handle , xTimeWAIT_FOREVER ) == pdPASS ){
        asm("BTG    LATA,4");
    }else{
        return;
    }
}}

void Task1(void){
    /// Do something usefull
    vTaskDelay( TSK1_DELAY );
}

void Task2(void){
    /// Do something usefull
    vTaskDelay( TSK2_DELAY );
}

void Timer0(void){
    xSemaphoreGive( Sem0_Handle );
}

/**
 * Interrupcion de baja prioridad.
 * @param   void
 * @return  void
 */
void __interrupt (high_priority) HIGH_ISR(void){
    
}
