#include "task.h"
#include "mem.h"
#include "List.h"
#include "port.h"

//<editor-fold defaultstate="collapsed" desc="COLAS DEL SISTEMA">
/// Lista de tareas listas para ejecutar ordenadas por prioridad
    volatile List_t     tskReadyQueue[tskPRIORITY_LEN]; //<- Tareas listas
/// Lista de tareas bloqueadas indefinidamente
    volatile List_t     tskBlockedQueue;                //<- Eventos async
/// Lista de tareas bloqueadas por un delay
    volatile List_t     tskWaitingQueue;                //<- Delay, Clock
/// Lista de tareas terminadas. Para eliminar en algun momento.
    volatile List_t     tskDoneQueue;                   //<- Tareas terminadas
//</editor-fold>
//<editor-fold defaultstate="collapsed" desc="VARIABLES PRIVADAS DEL SO">
    volatile TaskHandle_t       tskRunning;         //<- TAREA CORRIENDO.
    volatile TickType_t         tskSysTick;         //<- SYSTEM TICK REGISTER.
    volatile uint8_t            tskSchedulerStatus; //<- SCHEDULER STATUS.
//</editor-fold>

void __vTask_ERROR_HANDLER(void){
    vTaskDISABLE_INTERRUPTS();
    for(;;);
}
    
/// Iniciar los registros del OS a un estado conocido
void vTaskSetup( void ){
    UBaseType_t    counter;
    for(counter=0; counter<tskPRIORITY_LEN; counter++){
        vListInitialise( (List_Handle) &tskReadyQueue[counter] );
    }
    /// Inicializar las colas especiales
    vListInitialise( (List_Handle) &tskWaitingQueue );
    vListInitialise( (List_Handle) &tskBlockedQueue );
    vListInitialise( (List_Handle) &tskDoneQueue );
    tskSysTick = 0;
    tskSchedulerStatus = SCHEDULER_SLEEP;
}


/**
 * vTaskStartScheduler
 * Arrancar el calendarizador de tareas,
 * esta funcion no debe retornar, si lo
 * hace significa que ha habido un error.
 * Despues del llamado se ejecutara la
 * primer tarea de mas alta prioridad.
 * DEBE INVOCARSE UNA SOLA VEZ DESPUES DEL
 * RESET Y AL FINAL DE LA FUNCION MAIN. 
 * @param   void
 * @return  void
 */
void vTaskStartScheduler(void){
    /// Restaurar SysTick
    tskSysTick = 0;
    /// CONFIGURAR EL Hardware RT
    vCPU_CONFIG_SYSTICK();
    vCPU_START_SYSTICK();
    vTaskENABLE_INTERRUPTS();

    /// Llamar al Dispatcher y rezar por que todo funcione!
    vTaskDispatcher();
}

/// Crear una tarea de forma dinamica.
TaskHandle_t xTaskCreate( TaskFunction_t  pvTaskCode,
                        UBaseType_t     uxPriority){
//////////////////////////////////////////////
// VARIABLES UTILIZADAS POR LA FUNCION
//////////////////////////////////////////////
    TaskHandle_t     temp_task=0x00;
//////////////////////////////////////////////
//// VALIDACIONES PREVIAS A LA CONSTRUCCION
//////////////////////////////////////////////
    // Verificar que la prioridad sea correcta
    if( uxPriority >= tskPRIORITY_LEN ){return 0x00;}
    // Verificar que la funcion exista
    if( !pvTaskCode ){return 0x00;}
//////////////////////////////////////////////
//// CONSTRUCCION DE LA TAREA
//////////////////////////////////////////////
    /// Solicitar espacio en Heap.
    temp_task = (TaskHandle_t) malloc( sizeof( Task_t ) );
    /// Si no hay espacio suficiente, regresar un error.
    if( !temp_task ){return 0x00;}
//////////////////////////////////////////////
//// INICIALIZACION DE LA TAREA
////////////////////////////////////////////// 
    temp_task->function         = pvTaskCode;
    temp_task->ctl.priority     = uxPriority;
    temp_task->ctl.state        = TASK_START;
    temp_task->ctl.stopwatch    = 0;
        /// Inicializar el nodo para Cola de procesos
        vListInitialiseItem( &temp_task->ctl.ListNode );
        temp_task->ctl.ListNode.pvData  = temp_task;
        /// Inicializar el nodo para Cola de semaforos
        vListInitialiseItem( &temp_task->ctl.semNode );
        temp_task->ctl.semNode.pvData   = temp_task;
//////////////////////////////////////////////
//// REGISTRO DE LA TAREA
////////////////////////////////////////////// 
    /// Insertar tarea en lista ready correspondiente
    uxListInsertEnd( (List_t*) &tskReadyQueue[uxPriority] , (ListItem_t*) (&temp_task->ctl.ListNode) );
    return temp_task;
}
/// Crear una tarea de forma estatica.
TaskHandle_t xTaskCreateStatic( TaskFunction_t pvTaskCode,
                                UBaseType_t     uxPriority,
                                Task_t          *pxTaskBuffer ){
//////////////////////////////////////////////
//// VALIDACIONES PREVIAS A LA CONSTRUCCION
//////////////////////////////////////////////
    // Verificar que la prioridad sea correcta
    if( uxPriority >= tskPRIORITY_LEN ){return 0x00;}
    // Verificar que la funcion exista
    if( !pvTaskCode ){return 0x00;}
    /// Verificar si el buffer existe.
    if( !pxTaskBuffer ){return 0x00;}
//////////////////////////////////////////////
//// INICIALIZACION DE LA TAREA
////////////////////////////////////////////// 
    pxTaskBuffer->function         = pvTaskCode;
    pxTaskBuffer->ctl.priority     = uxPriority;
    pxTaskBuffer->ctl.state        = TASK_START;
    pxTaskBuffer->ctl.stopwatch    = 0;
        /// Inicializar el nodo para Cola de procesos
        vListInitialiseItem( &pxTaskBuffer->ctl.ListNode );
        pxTaskBuffer->ctl.ListNode.pvData  = &pxTaskBuffer;
        /// Inicializar el nodo para Cola de semaforos
        vListInitialiseItem( &pxTaskBuffer->ctl.semNode );
        pxTaskBuffer->ctl.semNode.pvData   = &pxTaskBuffer;
//////////////////////////////////////////////
//// REGISTRO DE LA TAREA
////////////////////////////////////////////// 
    /// Insertar tarea en lista ready correspondiente
    uxListInsertEnd( (List_t*) &tskReadyQueue[uxPriority] , (ListItem_t*) (&pxTaskBuffer->ctl.ListNode) );
    return pxTaskBuffer;
}
/// Encolar la tarea actual y ejecutar alguna otra de misma prioridad.
void vTaskYIELD( void ){
    vTaskENTER_CRITICAL();
    /// Activar las banderas de control.
        tskRunning->ctl.state      = TASK_READY;
        uxListInsertEnd( (List_Handle)&tskReadyQueue[ tskRunning->ctl.priority ],
                (ListItem_Handle) &tskRunning->ctl.ListNode );
    /// Cargar el despachador.
    vTaskEXIT_CRITICAL();
    return;
}
/// Suspender la tarea actual por un periodo determinado.
void vTaskDelay( TickType_t xTicksToDelay ){
    vTaskENTER_CRITICAL();
    /// Activar las banderas de control
    if( xTicksToDelay == xTimeWAIT_FOREVER ){
        /// Esperar por tiempo indefinido
        tskRunning->ctl.state      = TASK_BLOCKED;
        uxListInsertEnd( (List_Handle)&tskBlockedQueue,
                    (ListItem_Handle) &tskRunning->ctl.ListNode );
    }else if( xTicksToDelay == 0x00 ){
        /// Zero systicks equals to yielding
        tskRunning->ctl.state      = TASK_READY;
        uxListInsertEnd( (List_Handle)&tskReadyQueue[ tskRunning->ctl.priority ],
                (ListItem_Handle) &tskRunning->ctl.ListNode );
    }else{
        /// Esperar por tiempo definido
        tskRunning->ctl.state      = TASK_WAITING;
        //tskRunning->ctl.stopwatch  = xTicksToDelay;
        tskRunning->ctl.stopwatch  = tskSysTick + xTicksToDelay;
        uxListInsertEnd( (List_Handle)&tskWaitingQueue,
                    (ListItem_Handle) &tskRunning->ctl.ListNode );
    }
    /// Cargar el despachador.
    vTaskEXIT_CRITICAL();
}
/// Activar una tarea suspendida.
void vTaskResume( TaskHandle_t pxTaskToResume ){
    vTaskENTER_CRITICAL();
        /// Solo si TaskToResume no es NULL
        if(pxTaskToResume){
            /// Solo si la tarea no esta en estado ready actualmente
            if(pxTaskToResume->ctl.state != TASK_READY){
                pxTaskToResume->ctl.state = TASK_READY;
                /// Quitar de la cola actual
                uxListRemove( (ListItem_Handle) &pxTaskToResume->ctl.ListNode );
                /// Agregar a la cola READY
                uxListInsertEnd(
                            (List_Handle)&(tskReadyQueue[ pxTaskToResume->ctl.priority ]) ,
                            (ListItem_Handle) &pxTaskToResume->ctl.ListNode
                        );
            }
        }
    vTaskEXIT_CRITICAL();
}
/// Activar todas las tareas.
BaseType_t xTaskResumeAll( void ){
    vTaskENTER_CRITICAL();
        ListItem_Handle pNode;
        ListItem_Handle pNode_temp;
        TaskHandle_t    ptskNode;
            pNode       = (ListItem_Handle)tskBlockedQueue.pvRoot;
            pNode_temp  = (ListItem_Handle)pNode;
            while( pNode_temp ){
                /// Obtener el TaskHandle del nodo
                ptskNode = ((TaskHandle_t)pNode_temp->pvData);
                /// Apuntar a la siguiente tarea
                pNode = ((ListItem_Handle)pNode_temp->pvNext);
                    ptskNode->ctl.state = TASK_READY;
                    /// Quitar de la cola actual
                    uxListRemove( (ListItem_Handle) pNode_temp );
                    /// Agregar a la cola READY
                    uxListInsertEnd(
                                (List_Handle)&(tskReadyQueue[ ptskNode->ctl.priority ]) ,
                                (ListItem_Handle)pNode_temp
                            );
                /// Avanzamos en la lista
                pNode_temp = pNode;
            }
    vTaskEXIT_CRITICAL();
    return pdPASS;
}
/// Suspender una tarea.
void vTaskSuspend( TaskHandle_t pxTaskToSuspend ){
    vTaskENTER_CRITICAL();
        /// Solo si TaskToSuspend no es NULL
        if(!pxTaskToSuspend){   return;   }
            /// Solo si el Task no esta bloqueada
            if(pxTaskToSuspend->ctl.state != TASK_BLOCKED){
                pxTaskToSuspend->ctl.state = TASK_BLOCKED;
                /// Quitar de la lista Actual
                uxListRemove( (ListItem_Handle) &pxTaskToSuspend->ctl.ListNode );
                /// Agregar a la lista BLOCKED
                uxListInsertEnd(
                            (List_Handle)&(tskBlockedQueue) ,
                            (ListItem_Handle)&pxTaskToSuspend->ctl.ListNode
                        );
            }
    vTaskEXIT_CRITICAL();
}
/// Suspender todas las tareas listas.
void vTaskSuspendAll( void ){
    vTaskENTER_CRITICAL();
        ListItem_Handle pNode;
        ListItem_Handle pNode_temp;
        TaskHandle_t    ptskNode;
        uint8_t         counter=0;
        /// Remover todas las tareas en estado READY
        for(counter=0;counter<tskPRIORITY_LEN;counter++){
            pNode       = (ListItem_Handle)tskReadyQueue[counter].pvRoot;
            pNode_temp  = (ListItem_Handle)pNode;
            while( pNode_temp ){
                /// Obtener el TaskHandle del nodo
                ptskNode = ((TaskHandle_t)pNode_temp->pvData);
                /// Apuntar a la siguiente tarea
                pNode = ((ListItem_Handle)pNode_temp->pvNext);
                    /// Quitar de la lista READY
                    ptskNode->ctl.state = TASK_BLOCKED;
                    uxListRemove( (ListItem_Handle) &ptskNode->ctl.ListNode );
                    /// Agregar a la lista BLOCKED
                    uxListInsertEnd(
                                (List_Handle)&(tskBlockedQueue) ,
                                (ListItem_Handle)&ptskNode->ctl.ListNode
                            );
                /// Avanzamos en la lista
                pNode_temp = pNode;
            }
        }
        /// Romover todas las tareas en estado WAITING
        pNode       = (ListItem_Handle)tskWaitingQueue.pvRoot;
        pNode_temp  = (ListItem_Handle)pNode;
        while( pNode_temp ){
            /// Obtener el TaskHandle del nodo
            ptskNode = ((TaskHandle_t)pNode_temp->pvData);
            /// Apuntar a la siguiente tarea
            pNode = ((ListItem_Handle)pNode_temp->pvNext);
                /// Quitar de la lista READY
                ptskNode->ctl.state = TASK_BLOCKED;
                uxListRemove( (ListItem_Handle) &ptskNode->ctl.ListNode );
                /// Agregar a la lista BLOCKED
                uxListInsertEnd(
                            (List_Handle)&(tskBlockedQueue) ,
                            (ListItem_Handle) &ptskNode->ctl.ListNode
                        );
            /// Avanzamos en la lista
            pNode_temp = pNode;
        }
    vTaskEXIT_CRITICAL();
}
  
  
    /*-----------------------------------------------------------
    * SCHEDULER INTERNALS AVAILABLE FOR PORTING PURPOSES
    *----------------------------------------------------------*/
BaseType_t xTaskIncrementTick(void){
    ListItem_Handle pNode;
    ListItem_Handle pNode_temp;
    TaskHandle_t    ptskNode;
    BaseType_t      status;
/// A menos que se use TimeSlicing, si es el caso.
/// xTaskIncrementTick decide si debe o no despertar una tarea
    tskSysTick++;
    /// Verificar el estado del StopWatch de la tarea.
    status=0;
    pNode       = (ListItem_Handle)tskWaitingQueue.pvRoot;
    pNode_temp  = (ListItem_Handle)tskWaitingQueue.pvRoot;
    while( pNode_temp ){
        /// Obtener el TaskHandle del nodo
        ptskNode = ((TaskHandle_t)pNode_temp->pvData);
        /// Apuntar a la siguiente tarea
        pNode = ((ListItem_Handle)pNode_temp->pvNext);
        ///Cuando la tarea alcanza el tiempo solicitado
        // Es removida de la lista de espera y agregada a
        //  la lista Ready correspondiente
        
        ///if( !(--ptskNode->ctl.stopwatch) ){
        if( tskSysTick == ptskNode->ctl.stopwatch ){
            status = 1;
            /// Quitar de la lista WAITING
            uxListRemove( pNode_temp );
            /// Agregar a la lista READY
            uxListInsertEnd(
                        (List_Handle)&(tskReadyQueue[ ptskNode->ctl.priority ]) ,
                        (ListItem_Handle)pNode_temp
                    );
        }
        /// Avanzamos en la lista
        pNode_temp = pNode;
    }
    return status;
}
    /**
     * ESTA FUNCION NO DEBE UTILIZARCE DESDE CODIGO DE APLICACION,
     * ESTA PROVISTO PARA IMPLEMENTAR PORTABILIDAD DEL SISTEMA.
     *
     * Elige a la tarea que debe ejecutarse y configura los apuntadores.
     */
void vTaskSwitchContext(void){
    vTaskENTER_CRITICAL();
    uint8_t counter;
    /// Buscar el primer elemento listo con precedencia a las prioridades altas
    counter = tskPRIORITY_LEN;
    while(counter--){
        if( tskReadyQueue[ counter ].size ){
            /// Cargar al buffer la tarea
            tskRunning = ((TaskHandle_t)tskReadyQueue[ counter ].pvRoot->pvData);
            /// Eliminar de la lista ready
            uxListRemove(&tskRunning->ctl.ListNode);
            vTaskEXIT_CRITICAL();
            return;
        }
    }
    /// No hay tarea disponible para ejecucion
    tskRunning = 0x00;
    vTaskEXIT_CRITICAL();
}
    /**
     * ESTA FUNCION NO DEBE UTILIZARCE DESDE CODIGO DE APLICACION,
     * ESTA PROVISTO PARA IMPLEMENTAR PORTABILIDAD DEL SISTEMA.
     * 
     * Realiza todas las operaciones necesarias para cargar la tarea
     * que apunta el sistema.
     */
void    vTaskDispatcher(void){
    vTaskSYSTEM_CONTEXT_RESET();
    for(;;){/// Olvidar cualquier llamado anterior
        tskSchedulerStatus = SCHEDULER_DISPATCH;
        /// Esta funcion siempre se ejecuta desde el contexto SYSTEM.
        /// SELECCIONAR LA TAREA DE MAYOR PRIORIDAD
        vTaskSwitchContext();
        tskSchedulerStatus = SCHEDULER_EXEC;
        if( !tskRunning ){
            continue;
        }
        tskRunning->ctl.state = TASK_RUNNING;
        
        vTaskEXEC_TASK();
        
        /// Task never used any interfacec to sleep or switch
        //  Register to done tasks queue
        if( tskRunning->ctl.state == TASK_RUNNING ){
            vTaskENTER_CRITICAL();
            tskRunning->ctl.state = TASK_DONE;
            uxListInsertEnd(
                        (List_Handle)&(tskDoneQueue) ,
                        (ListItem_Handle)tskRunning
                    );
            vTaskEXIT_CRITICAL();
        }
    }
}
