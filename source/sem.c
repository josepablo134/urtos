#include "sem.h"
#include "task.h"
#include "port.h"
#include "mem.h"

extern  TaskHandle_t       tskRunning;         //<- TAREA CORRIENDO.
    
/// Create a binary Semaphore from a dynamic buffer
SemaphoreHandle_t  xSemaphoreCreateBinary( void )
{
    SemaphoreHandle_t   semTemp = \
            (SemaphoreHandle_t) malloc( sizeof( Semaphore_t ) );
    if( !semTemp ){ return 0x00; }
    semTemp->count      = 0x00;
    semTemp->maxCount   = 1;
    semTemp->holder     = 0x00;
    semTemp->type       = Semaphore_TypeBinary;
    return semTemp;
}

/// Create a binary Semaphore from a static buffer
SemaphoreHandle_t xSemaphoreCreateBinaryStatic( \
                            StaticSemaphore_t *pxSemaphoreBuffer )
{
    if( !pxSemaphoreBuffer ){ return 0x00; }
    pxSemaphoreBuffer->count    = 0x00;
    pxSemaphoreBuffer->maxCount = 1;
    pxSemaphoreBuffer->holder   = 0x00;
    pxSemaphoreBuffer->type     = Semaphore_TypeBinary;
    return pxSemaphoreBuffer;
}

/// Create a Counting Semaphore from a dynamic buffer
SemaphoreHandle_t xSemaphoreCreateCounting( \
                            UBaseType_t uxMaxCount,\
                            UBaseType_t uxInitialCount)
{
    SemaphoreHandle_t   semTemp = \
            (SemaphoreHandle_t) malloc( sizeof( Semaphore_t ) );
    if( !semTemp ){ return 0x00; }
    semTemp->count      = uxInitialCount;
    semTemp->maxCount   = uxMaxCount;
    semTemp->holder     = 0x00;
    semTemp->type       = Semaphore_TypeCounting;
    return semTemp;
}

/// Create a Counting Semaphore from a static buffer
SemaphoreHandle_t xSemaphoreCreateCountingStatic( \
                            UBaseType_t uxMaxCount,\
                            UBaseType_t uxInitialCount,\
                            StaticSemaphore_t *pxSemaphoreBuffer )
{
    if( !pxSemaphoreBuffer ){ return 0x00; }
    pxSemaphoreBuffer->count    = uxInitialCount;
    pxSemaphoreBuffer->maxCount = uxMaxCount;
    pxSemaphoreBuffer->holder   = 0x00;
    pxSemaphoreBuffer->type     = Semaphore_TypeCounting;

    return pxSemaphoreBuffer;
}

/// Create a Mutex Semaphore from a dynamic buffer
SemaphoreHandle_t xSemaphoreCreateMutex( void )
{
    SemaphoreHandle_t   semTemp = \
            (SemaphoreHandle_t) malloc( sizeof( Semaphore_t ) );
    if( !semTemp ){ return 0x00; }
    semTemp->count      = 0x0;
    semTemp->maxCount   = 1;
    semTemp->holder     = 0x0;
    semTemp->type       = Semaphore_TypeMutex;
    return semTemp;
}

/// Create a Mutex Semaphore from a static buffer
SemaphoreHandle_t xSemaphoreCreateMutexStatic( \
                            StaticSemaphore_t *pxMutexBuffer )
{
    if( !pxMutexBuffer ){ return 0x00; }
    pxMutexBuffer->count    = 0x00;
    pxMutexBuffer->maxCount = 1;
    pxMutexBuffer->holder   = 0x00;
    pxMutexBuffer->type     = Semaphore_TypeMutex;
    return pxMutexBuffer;
}

/// Get actual count from a Semaphore
UBaseType_t uxSemaphoreGetCount( SemaphoreHandle_t xSemaphore )
{
    UBaseType_t     tempCount;
    if( !xSemaphore ){ return 0x00; }
    vTaskENTER_CRITICAL();
        tempCount = xSemaphore->count;
    vTaskEXIT_CRITICAL();
    return tempCount;
}

/// Get TaskHandler that holds this Mutex
TaskHandle_t xSemaphoreGetMutexHolder( SemaphoreHandle_t xMutex )
{
    TaskHandle_t    tempHandle;
    if( !xMutex ){ return 0x00; }
    vTaskENTER_CRITICAL();
        tempHandle = xMutex->holder;
    vTaskEXIT_CRITICAL();
    return tempHandle;
}

/// Give a Semaphore count
BaseType_t xSemaphoreGive( SemaphoreHandle_t xSemaphore )
{
    if( !xSemaphore ){ return pdFAIL; }
    vTaskENTER_CRITICAL();
    switch( xSemaphore->type ){
        case Semaphore_TypeBinary:
            if( xSemaphore->count ){ goto ERROR; }
            xSemaphore->count++;
            if( xSemaphore->holder ){
                vTaskResume(xSemaphore->holder);
            }
            break;
        case Semaphore_TypeMutex:
            if( xSemaphore->count || xSemaphore->holder != tskRunning )
            { goto ERROR; }
            xSemaphore->holder = 0x00;
            xSemaphore->count++;
            break;
        case Semaphore_TypeCounting:
            if( xSemaphore->count < xSemaphore->maxCount ){
                xSemaphore->count++;
            }else{
                goto ERROR;
            }
            break;
        default:
            break;
    }
    vTaskEXIT_CRITICAL();
    return pdPASS;
ERROR:
    vTaskEXIT_CRITICAL();
    return pdFAIL;
}

/// Take a Semaphore count
BaseType_t xSemaphoreTake(SemaphoreHandle_t xSemaphore,\
                            TickType_t xTicksToWait )
{
    if( !xSemaphore ){ return pdFAIL; }
    vTaskENTER_CRITICAL();
    /// Goto to error if there is not sem available
    if( !xSemaphore->count ){
        xSemaphore->holder = tskRunning;
        vTaskDelay( xTicksToWait );
        goto ERROR;
    }
    switch( xSemaphore->type ){
        case Semaphore_TypeBinary:
        case Semaphore_TypeCounting:
            xSemaphore->count--;
            break;
        case Semaphore_TypeMutex:
            xSemaphore->holder = tskRunning;
            xSemaphore->count--;
            break;
        default:
            break;
    }
    vTaskEXIT_CRITICAL();
    return pdPASS;
ERROR:
    vTaskEXIT_CRITICAL();
    return pdFAIL;
}
