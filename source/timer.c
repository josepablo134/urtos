#include "timer.h"
#include "mem.h"
#include "task.h"
#include "port.h"
#include "List.h"

//<editor-fold defaultstate="collapsed" desc="COLAS DEL SISTEMA">
    volatile List_t     tmrBlockedQueue;
    volatile List_t     tmrWaitingQueue;
    volatile List_t     tmrReadyQueue;
    
    extern   List_t     tskReadyQueue[ tskPRIORITY_LEN ];
    extern   List_t     tskWaitingQueue;
//</editor-fold>
//<editor-fold defaultstate="collapsed" desc="VARIABLES PRIVADAS DEL SO">
    volatile TimerHandle_t          tmrRunning; /// Timer obj
    volatile TaskHandle_t           tmrDaemon;  /// Task Handle
    extern   TickType_t             tskSysTick;
//</editor-fold>

/// Sort timer queue
//  1.- Find next period time.
//  2.- Move all timers of equal period to min period, into Ready queue
//  3.- Sort waiting queue by descending order
void vTimerSort(void);
    
void __tmrDaemon__(void){
    TickType_t              actualTime;
    vTaskENTER_CRITICAL();
        /// Guardar tiempo actual
        actualTime  = tskSysTick;
    vTaskEXIT_CRITICAL();
    
    /// Pick a timer process and run it
    while( tmrReadyQueue.size ){
        tmrRunning = (TimerHandle_t) tmrReadyQueue.pvRoot->pvData;
        uxListRemove( (ListItem_t*) tmrRunning->ListNode );
        tmrRunning->state = TIMER_RUNNING;
        vTimerEXEC_TMR();
        /// Auto reload if needed
        if( tmrRunning->autoreload == pdTRUE ){
            /// Register again timer process
            tmrRunning->exectime    = actualTime + tmrRunning->period;
            tmrRunning->state       = TIMER_WAITING;
            uxListInsertEnd( (List_t*)&tmrWaitingQueue ,
                    (ListItem_t*) tmrRunning->ListNode );
        }else{
            tmrRunning->state = TIMER_DONE;
            uxListInsertEnd( (List_t*)&tmrBlockedQueue ,
                    (ListItem_t*) tmrRunning->ListNode );
        }
    }
    
    /// Sort timer Queue
    vTimerSort();
    
    if( tmrReadyQueue.size == 0 ){ 
        return vTaskDelay( xTimeWAIT_FOREVER );
    }
    
    /// Calculate next execution period
    // All ready elements are at equal period, next execution period
    actualTime = ((TimerHandle_t)(tmrReadyQueue.pvRoot->pvData))->exectime - \
                    actualTime;
    /// Request execution period
    vTaskDelay( actualTime );
}
    
/// Inicializar los registros del SO a un estado conocido.
BaseType_t xTimerSetup(void){
    /// Inicializar las colas especiales
    vListInitialise( (List_Handle) &tmrWaitingQueue );
    vListInitialise( (List_Handle) &tmrBlockedQueue );
    vListInitialise( (List_Handle) &tmrReadyQueue );
    
    /// Crear el demonio
    tmrDaemon = xTaskCreate(   __tmrDaemon__ ,       ///callback al proceso
                tskHIGH_PRIORITY );       ///Tarea de mas alta prioridad
    
    if( !tmrDaemon ){ return pdFAIL; }
    /// No timer ready
    tmrRunning = 0x00;
    return pdPASS;

    /// Para que el Compilador sea feliz e incluya las funciones
    /// {    
    __tmrDaemon__();
    /// }
    /// Para que el Compilador sea feliz e incluya las funciones
}

/// Crear un timer de forma dinamica.
TimerHandle_t xTimerCreate( TickType_t xTimerPeriod,
                            UBaseType_t uxAutoReload,
                            TimerCallbackFunction_t pxCallbackFunction ){
    /// Reservar memoria para el timer
    TimerHandle_t       timer_temp=0x00;
    ListItem_Handle     tmrNode=0x00;
    
    if( !pxCallbackFunction ){ return 0x00; }// No callback
    
    timer_temp = (TimerHandle_t) malloc( sizeof( StaticTimer_t ) );
    if(!timer_temp){ return 0x00; }/// No hay memoria suficiente
    
    tmrNode = (ListItem_Handle) malloc( sizeof( ListItem_t ) );
    if(!tmrNode){ return 0x00; }/// No hay memoria suficiente
    
    /////////////////////////////////////
    //  Inicializar timer
    /////////////////////////////////////
    timer_temp->function    = pxCallbackFunction;
    timer_temp->period      = xTimerPeriod;
    timer_temp->timeout     = 0;
    timer_temp->exectime    = 0;
    timer_temp->autoreload  = uxAutoReload;
    timer_temp->state       = TIMER_BLOCKED;
    /////////////////////////////////////
    //  Inicializar nodo y registrarlo
    /////////////////////////////////////
    timer_temp->ListNode    = tmrNode;
    tmrNode->pvData         = timer_temp;
    uxListInsertEnd( (List_t*) &tmrBlockedQueue, (ListItem_t*) tmrNode );
    return timer_temp;
}
/// Crear un timer de forma estatica.
TimerHandle_t xTimerCreateStatic( TickType_t xTimerPeriod,
                                UBaseType_t uxAutoReload,
                                TimerCallbackFunction_t pxCallbackFunction,
                                StaticTimer_t *pxTimerBuffer  ){
    ListItem_Handle     tmrNode=0x00;
    if( !pxTimerBuffer ){ return 0x00; }        //  No buffer
    if( !pxCallbackFunction ){ return 0x00; }   //  No callback
    tmrNode = (ListItem_Handle) malloc( sizeof( ListItem_t ) );
    if(!tmrNode){ return 0x00; }/// No hay memoria suficiente
    
    /////////////////////////////////////
    //  Inicializar timer
    /////////////////////////////////////
    pxTimerBuffer->function    = pxCallbackFunction;
    pxTimerBuffer->period      = xTimerPeriod;
    pxTimerBuffer->timeout     = 0;
    pxTimerBuffer->exectime    = 0;
    pxTimerBuffer->autoreload  = uxAutoReload;
    pxTimerBuffer->state       = TIMER_BLOCKED;
    
    /////////////////////////////////////
    //  Inicializar nodo y registrarlo
    /////////////////////////////////////
    pxTimerBuffer->ListNode     = tmrNode;
    tmrNode->pvData             = pxTimerBuffer;
    uxListInsertEnd( (List_t*) &tmrBlockedQueue, (ListItem_t*) tmrNode );
    
    return pxTimerBuffer;
}
/// Comenzar el conteo del timer.
/// No puede ser usado desde un proceso timer
BaseType_t xTimerStart( TimerHandle_t xTimer, TickType_t xTicksToWait ){
    if( !xTimer ){ return pdFAIL; }
    vTaskENTER_CRITICAL();/// Evitar que tskSysTick cambie
        /// Calcular el periodo de ejecucion
        xTimer->exectime = xTicksToWait + tskSysTick;
    vTaskEXIT_CRITICAL();

    /// Registrar proceso timer
    uxListRemove( (ListItem_Handle)xTimer->ListNode );
    xTimer->state = TIMER_WAITING;
    uxListInsertEnd( (List_Handle)&tmrWaitingQueue ,
                (ListItem_Handle)xTimer->ListNode );
    
    /// Ordenar colas de timer
    vTimerSort();/// Despues de este llamado, debe haber por lo menos
                 //  un timer en cola, el peor de los casos es este mismo timer

    vTaskENTER_CRITICAL();
        /// Modificar temporizador del demonio
        tmrDaemon->ctl.stopwatch = \
            ((TimerHandle_t)tmrReadyQueue.pvRoot->pvData)->exectime;
            /// Registrar en cola correspondiente
            uxListRemove( &tmrDaemon->ctl.ListNode );
        if( xTicksToWait ){
            /// Esperar por tiempo definido
            tmrDaemon->ctl.state      = TASK_WAITING;
            uxListInsertEnd( (List_Handle)&tskWaitingQueue,
                (ListItem_Handle) &tmrDaemon->ctl.ListNode );
        }else{
            /// Esperar por tiempo definido
            tmrDaemon->ctl.state      = TASK_READY;
            uxListInsertEnd( (List_Handle)&(tskReadyQueue[ tskHIGH_PRIORITY ]),
                (ListItem_Handle) &tmrDaemon->ctl.ListNode );
        }
    vTaskEXIT_CRITICAL();
    return pdPASS;
}
/// Detener el conteo del timer.
/// No puede ser usado desde un proceso timer
BaseType_t xTimerStop( TimerHandle_t xTimer){
    if( !xTimer ){ return pdFAIL; }
    
    /// Registrar proceso timer en cola bloqueada
    uxListRemove( (ListItem_Handle)xTimer->ListNode );
    xTimer->state   =   TIMER_BLOCKED;
    uxListInsertEnd( (List_Handle)&tmrBlockedQueue ,
                (ListItem_Handle)xTimer->ListNode );
        
            /// Ordenar colas de timer
    vTimerSort();/// Despues de este llamado, puede ser que no haya timers
                 // por procesar

    vTaskENTER_CRITICAL();
        if( tmrReadyQueue.size == 0 ){
            vTaskSuspend( (TaskHandle_t) tmrDaemon );
        }else{
            /// Existe la posibilidad de que el nuevo periodo haya cambiado
            
            //  Modificar temporizador del demonio
            tmrDaemon->ctl.stopwatch = \
                ((TimerHandle_t)tmrReadyQueue.pvRoot->pvData)->exectime;
            //  Esperar por tiempo definido
            tmrDaemon->ctl.state      = TASK_WAITING;
            //  Registrar en cola correspondiente
            uxListRemove( &tmrDaemon->ctl.ListNode );
            uxListInsertEnd( (List_Handle)&tskWaitingQueue,
                (ListItem_Handle) &tmrDaemon->ctl.ListNode );
        }
    vTaskEXIT_CRITICAL();
        
    return pdPASS;
}

/// Sort timer queue
//  1.- Find next period time.
//  2.- Move all timers of equal period to min period, into Ready queue
//  O se ejecuta desde el demonio, o se ejecuta desde un contexto ajeno,
//  Para el caso del contexto ajeno, este debe desactivar al demonio
//  Existe la posibilidad de haber Timers Ready esperando, con mayor periodo,
//  entonces, es necesario mover todos estos a cola waiting
void vTimerSort(){
    ListItem_Handle     nodeTemp,nextNodeTemp;
    TickType_t          minTime;
    if( tmrWaitingQueue.size == 0 ){ return; }
    
    minTime     = xTimeWAIT_FOREVER;
    nodeTemp    = (ListItem_Handle)(tmrWaitingQueue.pvRoot);
    /// Encontrar el minimo periodo siguiente
    while( nodeTemp ){
        if( minTime > ((TimerHandle_t)nodeTemp->pvData)->exectime ){
            minTime = ((TimerHandle_t)nodeTemp->pvData)->exectime;
        }
        nodeTemp = (ListItem_Handle)nodeTemp->pvNext;
    }
    
    /// Si hay elementos en la cola ready, con periodo de ejecucion menor,
    //  re ordenar.
    if( tmrReadyQueue.size ){
        if( minTime < ((TimerHandle_t)tmrReadyQueue.pvRoot->pvData)->exectime  ){
            nodeTemp = tmrReadyQueue.pvRoot;
            while( nodeTemp ){
                nextNodeTemp = nodeTemp->pvNext;
                uxListRemove( (ListItem_Handle)nodeTemp );
                uxListInsertEnd( (List_Handle) &tmrWaitingQueue
                        , (ListItem_Handle)nodeTemp );
                nodeTemp = nextNodeTemp;
            }
        }
    }
    
    nodeTemp    = (ListItem_Handle)(tmrWaitingQueue.pvRoot);
    /// Mover todos los timers con el minimo periodo a cola Ready
    while( nodeTemp ){
        nextNodeTemp = (ListItem_Handle) nodeTemp->pvNext;
        if( minTime == ((TimerHandle_t)nodeTemp->pvData)->exectime ){
            uxListRemove( (ListItem_Handle) nodeTemp );
            uxListInsertEnd( (List_Handle)&tmrReadyQueue ,
                            (ListItem_Handle)nodeTemp );
        }
        nodeTemp = nextNodeTemp;
    }
    return;
}
