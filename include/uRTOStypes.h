#ifndef _RTOS_TYPES_H_
#define _RTOS_TYPES_H_
    #include <stdint.h>
    #include "port.h"
    ///////////////////////////////////
    //  BASIC TYPES
    ///////////////////////////////////
    typedef portUBaseType_t UBaseType_t;
    typedef portUBaseType_t BaseType_t;
    typedef portTickType_t  TickType_t;
    ///////////////////////////////////
    //  LIST TYPES
    ///////////////////////////////////
    typedef struct ListItem_t{
        struct ListItem_t*      pvLast;
        void*                   pvData;
        void*                   pvList;
        struct ListItem_t*      pvNext;
    }ListItem_t;
    typedef struct List_t{
        ListItem_t*             pvRoot;
        ListItem_t*             pvLast;
        uint16_t                size;
    }List_t;
    typedef List_t* List_Handle;
    typedef ListItem_t* ListItem_Handle;

    ///////////////////////////////////
    //  TASK TYPES
    ///////////////////////////////////
    typedef enum SCHEDULER_STATE{
        SCHEDULER_SLEEP=0,
        SCHEDULER_DISPATCH,
        SCHEDULER_EXEC,
        SCHEDULER_STATES
    }SCHEDULER_STATE;
    typedef enum TASK_STATE{
		TASK_START=0,   //<- La tarea no ha sido ejecutada. Esta lista.
		TASK_READY,     //<- La tarea ha sido ejecutada y esta lista.
		TASK_WAITING,   //<- La tarea espera por un evento sincrono.
		TASK_RUNNING,   //<- La tarea esta en ejecucion.
        TASK_BLOCKED,   //<- La tarea espera por un evento asincrono.
		TASK_DONE       //<- La tarea ha sido finalizada.
	}TASK_STATE;
    typedef enum TaskPriority_t{
        tskIDLE_PRIORITY=0,     //<-Mas baja prioridad.
        tskLOW_PRIORITY,        //<-Prioridad Baja.
        tskNORMAL_PRIORITY,     //<-Prioridad Normal.
        tskHIGH_PRIORITY,       //<-Mas alta prioridad.
        tskPRIORITY_LEN,
    }TaskPriority_t;
    typedef void(*TaskFunction_t)(void);
    typedef struct TaskControl_t{
        UBaseType_t     priority;       /// ID de prioridad
        UBaseType_t     state;          /// Estado de la tarea
        TickType_t      stopwatch;      /// Siguiente tiempo de ejecucion
        ListItem_t      ListNode;       /// Nodo para colas de procesos
        ListItem_t      semNode;        /// Nodo para semaforos
    }TaskControl_t;
    typedef struct Task_t{
        TaskControl_t       ctl;        //Estructura de control
		TaskFunction_t      function;   //Apuntador a funcion
                                        //void function(void*)
    }Task_t;
    typedef Task_t* TaskHandle_t;
    
    ///////////////////////////////////
    //  TIMER TYPES
    ///////////////////////////////////
    /*
     ** Por definir:
     * TimerHandle_t
     * TimerCallbackFunction_t
     * StaticTimer_t
    */
    typedef enum TIMER_STATE{
		TIMER_READY=0,   //<- La tarea no ha sido ejecutada. Esta lista.
		TIMER_WAITING,   //<- La tarea espera por un evento sincrono.
		TIMER_RUNNING,   //<- La tarea esta en ejecucion.
        TIMER_BLOCKED,   //<- La tarea espera por un evento asincrono.
		TIMER_DONE       //<- La tarea ha sido finalizada.
	}TIMER_STATE;
    typedef void(*TimerCallbackFunction_t)(void);
    typedef struct StaticTimer_t{
        TimerCallbackFunction_t     function;
        TickType_t                  period;
        TickType_t                  timeout;
        TickType_t                  exectime;
        UBaseType_t                 autoreload;
        UBaseType_t                 state;
        
        ListItem_Handle             ListNode;
    }StaticTimer_t;
    typedef StaticTimer_t* TimerHandle_t;
    
    ///////////////////////////////////
    //  SEMAPHORES TYPES
    ///////////////////////////////////
    /**Por definir:
     * SemaphoreHandle_t
     * StaticSemaphore_t
    */
    typedef enum Semaphore_Type{
        Semaphore_TypeBinary=0,
        Semaphore_TypeCounting,
        Semaphore_TypeMutex,
        Semaphore_All_Types
    }Semaphore_Type;
    typedef struct Semaphore_t{
        TaskHandle_t        holder;
        UBaseType_t         count;
        UBaseType_t         maxCount;
        Semaphore_Type      type;
    }Semaphore_t;
    typedef Semaphore_t*    SemaphoreHandle_t;
    typedef Semaphore_t     StaticSemaphore_t;

    #define pdTRUE              0x01
    #define pdFALSE             0x00

    #define pdPASS              0x00
    #define pdFAIL              0x01
  
    #define null                0x00

    #define xTimeWAIT_FOREVER       xPortWAIT_FOREVER
    #define MEM_MAN_MAX_HEAP_SIZE   1024    ///bytes
#endif
