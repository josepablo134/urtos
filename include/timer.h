/**
* @date     07/11/2018
* @version  1.0.0
* @autor    Josepablo Cruz Baas
* @description
*   Timer es una API para la creacion y
*   administracion de software timers, fuertemente
*   influenciada por FreeRTOS.
*/
#ifndef _TIMER_H_
#define _TIMER_H_
    /// Incluir los tipos de datos estandar del RTOS.
    #include "uRTOStypes.h"

    /**
     * vTimerSetup
     * Inicializar los registros del SO a un
     * estado conocido.
     * DEBE INVOCARSE ANTES QUE CUALQUIER OTRA FUNCION DEL Timer.
     * @param   void
     * @return  void
     */
    extern BaseType_t xTimerSetup(void);
    /**
     * xTimerCreate
     * Crear un software timer de forma dinamica,
     * esto significa que se reservara memoria
     * dinamica desde HEAP para crear las estructuras
     * necesarias del timer, luego se registrara en
     * la cola correspondiente.
     * @param xTimerPeriod      Periodo del timer en systicks.
     * @param uxAutoReload      Periodic exec(pdTRUE), or one shot execution(pdFALSE).
     * @param pxCallbackFunction La funcion a ejecutar de forma 
     * @return  El manejador al timer, si devuelve NULL hubo un error.
     */
    extern  TimerHandle_t xTimerCreate( TickType_t xTimerPeriod,
                                            UBaseType_t uxAutoReload,
                                            TimerCallbackFunction_t pxCallbackFunction );
    /// Crear un timer de forma estatica.
    extern  TimerHandle_t xTimerCreateStatic( TickType_t xTimerPeriod,
                                                UBaseType_t uxAutoReload,
                                                TimerCallbackFunction_t pxCallbackFunction,
                                                StaticTimer_t *pxTimerBuffer  );
    /// Comenzar el conteo del timer.
    extern  BaseType_t xTimerStart( TimerHandle_t xTimer, TickType_t xTicksToWait );
    /// Detener el conteo del timer.
    extern  BaseType_t xTimerStop( TimerHandle_t xTimer );
    
    #define vTimerEXEC_TMR(){   vPortEXEC_TMR();    }
#endif
