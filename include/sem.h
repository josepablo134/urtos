/**
* @date     07/11/2018
* @version  1.0.0
* @autor    Josepablo Cruz Baas
* @description
*   Timer es una API para la creacion y
*   administracion de semaforos, fuertemente
*   influenciada por FreeRTOS.
*/
#ifndef _SEM_H_
#define _SEM_H_
    /// Incluir los tipos de datos estandar del RTOS.
    #include "uRTOStypes.h"
    
    /// Create a binary Semaphore from a dynamic buffer
    extern  SemaphoreHandle_t  xSemaphoreCreateBinary( void );
    
    /// Create a binary Semaphore from a static buffer
    extern  SemaphoreHandle_t xSemaphoreCreateBinaryStatic( \
                                StaticSemaphore_t *pxSemaphoreBuffer );
    
    /// Create a Counting Semaphore from a dynamic buffer
    extern  SemaphoreHandle_t xSemaphoreCreateCounting( \
                                UBaseType_t uxMaxCount,\
                                UBaseType_t uxInitialCount);
    
    /// Create a Counting Semaphore from a static buffer
    extern  SemaphoreHandle_t xSemaphoreCreateCountingStatic( \
                                UBaseType_t uxMaxCount,\
                                UBaseType_t uxInitialCount,\
                                StaticSemaphore_t *pxSemaphoreBuffer );
    
    /// Create a Mutex Semaphore from a dynamic buffer
    extern  SemaphoreHandle_t xSemaphoreCreateMutex( void );
    
    /// Create a Mutex Semaphore from a static buffer
    extern  SemaphoreHandle_t xSemaphoreCreateMutexStatic( \
                                StaticSemaphore_t *pxMutexBuffer );
    
    /// Get actual count from a Semaphore
    extern  UBaseType_t uxSemaphoreGetCount( SemaphoreHandle_t xSemaphore );
    
    /// Get TaskHandler that holds this Mutex
    extern  TaskHandle_t xSemaphoreGetMutexHolder( SemaphoreHandle_t xMutex );
    
    /// Give a Semaphore count
    extern  BaseType_t xSemaphoreGive( SemaphoreHandle_t xSemaphore );
    
    /// Take a Semaphore count
    extern  BaseType_t xSemaphoreTake( SemaphoreHandle_t xSemaphore,\
                                        TickType_t xTicksToWait );
#endif
