/**
* @date     03/01/2019
* @version  1.0.0
* @autor    Josepablo Cruz Baas
* @description
*   Task es una API para la creacion y
*   administracion de tareas, fuertemente
*   influenciada por FreeRTOS.
*/
#ifndef _TASK_H_
#define _TASK_H_
    /// Incluir los tipos de datos estandar del RTOS.
	#include "uRTOStypes.h"
    /**
     * vTaskSetup
     * Inicializar los registros del SO a un
     * estado conocido.
     * DEBE INVOCARSE ANTES QUE CUALQUIER OTRA FUNCION DEL RTOS.
     * @param   void
     * @return  void
     */
    extern void vTaskSetup( void );
    /**
     * vTaskStartScheduler
     * Arrancar el calendarizador de tareas,
     * esta funcion no debe retornar, si lo
     * hace significa que ha habido un error.
     * Despues del llamado se ejecutara la
     * primer tarea de mas alta prioridad.
     * DEBE INVOCARSE UNA SOLA VEZ DESPUES DEL
     * RESET Y AL FINAL DE LA FUNCION MAIN. 
     * @param   void
     * @return  void
     */
    extern void vTaskStartScheduler( void );
    /**
     * xTaskCreate
     * Crear una tarea de forma dinamica,
     * esto significa que se reservara memoria
     * dinamica desde HEAP para crear las estructuras
     * necesarias de la tarea, luego se registrara en
     * la cola correspondiente.
     * @param pvTaskCode        Funcion a ejecutar como tarea.
     * @param id                Identificador de la tarea. (No es util al SO).
     * @param uxPriority        Prioridad de la tarea.
     * @param pxCreatedTask     Apuntador al que sera el manejador de la tarea.
     * @return  pdFAIL significa que ha habido un error,
     *          pdPASS significa que la operacion ha sido exitosa.
     */
    extern TaskHandle_t xTaskCreate(    TaskFunction_t     pvTaskCode,
                                        UBaseType_t     uxPriority );
    /**
     * xTaskCreateStatic
     * Crear una tarea de forma estatica,
     * esto significa que se inicializara
     * la estructura Task_t y se le registrara
     * en la cola correspondiente, sin embargo,
     * aunque no se reserva memoria HEAP
     * para la tarea, el registro si reserva
     * memoria HEAP.
     * @param pvTaskCode            Funcion a ejecutar como tarea.
     * @param id                    Identificador de la tarea. (No es util al SO).
     * @param uxPriority            Prioridad de la tarea.
     * @param pxTaskBuffer          Apuntador al que sera el manejador de la tarea.
     * @return  NULL significa que ha habido un error.
     */
    extern TaskHandle_t xTaskCreateStatic( TaskFunction_t pvTaskCode,
                                        UBaseType_t uxPriority,
                                        Task_t *pxTaskBuffer );
    /**
     * taskYIELD
     * Compartir tiempo o pasar ha siguiente
     * tarea, la tarea actual entrega tiempo
     * de ejecucion voluntariamente para la
     * siguiente tarea de misma prioridad
     * en cola, si no hay otra tarea, el
     * despachador retornara a esta misma.
     * 
     * ## Esta funcion solo puede ser usada desde el contexto de una tarea.
     * 
     * @param   void
     * @return  void
     */
    extern void vTaskYIELD( void );
    /**
     * vTaskDelay
     * Suspender la tarea actual por un tiempo
     * definido, lo que significa que la tarea
     * pasa a estado TASK_WAITING, cuando el
     * tiempo haya concluido la tarea pasa a
     * estado TASK_READY y es registrada en cola.
     * @param xTicksToDelay     Tiempo en Ticks que la tarea se suspendera.
     */
    extern void vTaskDelay( TickType_t xTicksToDelay );
    /**
     * vTaskResume
     * Despertar una tarea suspendida, solo
     * si la tarea esta en estado TASK_BLOCKED
     * sera despertada.
     * @param pxTaskToResume    Manejador de la tarea a despertar.
     */
    extern void vTaskResume( TaskHandle_t pxTaskToResume );
    /**
     * xTaskResumeAll
     * Despertar todas las tareas que esten en
     * estado TASK_BLOCKED, esto no afecta a
     * las demas tareas.
     * @return pdFAIL   Significa que ha habido algun error.
     */
    extern BaseType_t xTaskResumeAll( void );
    /**
     * vTaskSuspend
     * Suspender una tarea en estado TASK_READY,
     * significa que la tarea pasara a estado
     * TASK_BLOCKED y sera desencolada.
     * ## No usar con la tarea en ejecucion, para ello usar vTaskDelay.
     * @param pxTaskToSuspend   Manejador de la tarea a suspender.
     */
    extern void vTaskSuspend( TaskHandle_t pxTaskToSuspend );
    /**
     * vTaskSuspendAll
     * Suspender todas las tareas registradas en cola,
     * esto pasa todas las tareas TASK_READY y TASK_WAITING
     * a TASK_BLOCKED.
     * Esta funcion esta propuesta para activar modos de
     * ahorro de energia.
     * No suspende la tarea que la haya llamado.
     * @param void
     */
    extern void vTaskSuspendAll( void );
  
    /*-----------------------------------------------------------
    *    FUNCIONES RESERVADAS PARA EL USO DEL SCHEDULER
    *----------------------------------------------------------*/
    /*
    * ESTA FUNCION NO DEBE UTILIZARCE DESDE CODIGO DE APLICACION,
    * ESTA PROVISTO PARA IMPLEMENTAR PORTABILIDAD DEL SISTEMA.
    *
    * Llamado desde el kernel tick, esta funcion incrementa el
    * contador tskSysTick y verifica si alguna tarea en estado TASK_WAITING
    * deberia registrarse en cola. Si algun valor diferente de cero es
    * retornado entonces se requiere cambio de contexto por alguna de las
    * siguientes razones :
    *   + Alguna tarea ha sido encolada.
    *     or
    *   + Se requiere compartir tiempo con alguna tarea de igual prioridad.
    */
    extern  BaseType_t xTaskIncrementTick(void);
    /**
     * ESTA FUNCION NO DEBE UTILIZARCE DESDE CODIGO DE APLICACION,
     * ESTA PROVISTO PARA IMPLEMENTAR PORTABILIDAD DEL SISTEMA.
     *
     * Elige a la tarea que debe ejecutarse y configura los apuntadores.
     */
    extern  void vTaskSwitchContext(void);
    /**
     * ESTA FUNCION NO DEBE UTILIZARCE DESDE CODIGO DE APLICACION,
     * ESTA PROVISTO PARA IMPLEMENTAR PORTABILIDAD DEL SISTEMA.
     * 
     * Realiza todas las operaciones necesarias para cargar la tarea
     * que apunta el sistema.
     */
    extern  void vTaskDispatcher(void);
    
//<editor-fold defaultstate="collapsed" desc="FUNCIONES DEPENDIENTES DE LA ARQUITECTURA">
    
    #define     vTaskEXEC_TASK(){ vPortEXEC_TASK(); }
    
    #define     vTaskENTER_CRITICAL(){ vPortENTER_CRITICAL(); }
    #define     vTaskEXIT_CRITICAL(){ vPortEXIT_CRITICAL(); }
    
    #define     vTaskENABLE_INTERRUPTS(){ vPortDISABLE_INTERRUPTS(); }
    #define     vTaskDISABLE_INTERRUPTS(){ vPortENABLE_INTERRUPTS(); }
    
    #define     vTaskENTER_CRITICAL(){ vPortENTER_CRITICAL(); }
    #define     vTaskEXIT_CRITICAL(){ vPortEXIT_CRITICAL(); }

    #define     vTaskSYSTEM_CONTEXT_RESET(){ vPortSYSTEM_CONTEXT_RESET(); }
    
    #define     vCPU_SLEEP(){ vPortCPU_SLEEP(); }
    #define     vCPU_RESET(){ vPortCPU_RESET(); }

    #define     vCPU_CONFIG_SYSTICK(){ vPortCPU_CONFIG_SYSTICK(); }
    
    #define     vCPU_START_SYSTICK(){ vPortCPU_START_SYSTICK(); }
    #define     vCPU_STOP_SYSTICK(){ vPortCPU_STOP_SYSTICK(); }

    #define     pdMS_TO_TICKS           pdPORT_MS_TO_TICKS
//</editor-fold>
#endif
